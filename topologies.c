#include <stdio.h>
#include "graph.h"
#include "network.h"

// refer "Topology Schema" file for list of all topologies
glgraph_t *build_generic_graph()
{
	glgraph_t *topo = create_glgraph("Hello World Generic Graph");

	node_t *dev0 = create_glgraph_node(topo, "Dev0"),
		   *dev1 = create_glgraph_node(topo, "Dev1"),
		   *dev2 = create_glgraph_node(topo, "Dev2");

	insert_link(dev0, dev1, "eth0/0", "eth0/1", 1);
	insert_link(dev1, dev2, "eth0/2", "eth0/3", 1);
	insert_link(dev0, dev2, "eth0/4", "eth0/5", 1);

	set_loopback(dev0, "122.1.1.0");
	set_intf_ip(dev0, "eth0/0", "10.1.1.0", 24);
	set_intf_ip(dev0, "eth0/4", "10.1.1.1", 24);

	set_loopback(dev1, "122.1.1.1");
	set_intf_ip(dev1, "eth0/1", "20.1.1.0", 24);
	set_intf_ip(dev1, "eth0/2", "20.1.1.1", 24);
	
	set_loopback(dev2, "122.1.1.2");
	set_intf_ip(dev2, "eth0/3", "30.1.1.0", 24);
	set_intf_ip(dev2, "eth0/5", "30.1.1.1", 24);

	printf("build_generic_graph() done\n");
	return topo;
}

