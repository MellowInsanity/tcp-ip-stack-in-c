#include "utils.h"
#include "libglue/glnode.h"

void octet_to_int(unsigned char octet[3], unsigned int *dest)
{
	unsigned int digit = 0;
	int POT = -1;

	for (int i = 0; i < 3; i++) {
		if (octet[i] != STR_NULL)
			POT += 1;
	}

	if (POT == 0) {
		digit = (unsigned int)(octet[2] - '0');
		*dest += digit;
	}
	if (POT == 1) {
		digit = ((unsigned int)(octet[2] - '0')) * 10;
		*dest += digit;

		digit = (unsigned int)(octet[1] - '0');
		*dest += digit;
	}
	if (POT == 2) {
		digit = ((unsigned int)(octet[2] - '0')) * 100;
		*dest += digit;

		digit = ((unsigned int)(octet[1] - '0')) * 10;
		*dest += digit;

		digit = (unsigned int)(octet[0] - '0');
		*dest += digit;
	}
}


void append_octet(unsigned char *ip, unsigned char *octet, unsigned char delim) {
	int iplen = mystrlen(ip);
	
	ip[iplen] = octet[0];

	if (octet[1]){
		ip[iplen+1] = octet[1];
	}
	else{
		ip[iplen+1] = delim;

		if (delim != STR_NULL)
			ip[iplen+2] = STR_NULL;	

		return;	
	} 

	if (octet[2]) {
		ip[iplen+2] = octet[2];
	}
	else {
		ip[iplen+2] = delim;

		if (delim != STR_NULL)
			ip[iplen+3] = STR_NULL;

		return;
	}

	ip[iplen+3] = delim;

	if (delim != STR_NULL)
			ip[iplen+4] = STR_NULL;

	
}

void my_itoa(unsigned char bin_octet, unsigned char *str_octet)
{
	if ((bin_octet / 100) > 0) {
		str_octet[0] = (bin_octet / 100) + '0';
		bin_octet %= 100;
		str_octet[1] = (bin_octet / 10) + '0';
		bin_octet %= 10;
		str_octet[2] = (bin_octet) + '0';
	}
	else if ((bin_octet / 10) > 0) {
		str_octet[0] = (bin_octet / 10) + '0';
		bin_octet %= 10;
		str_octet[1] = (bin_octet) + '0';
	}
	else {
		str_octet[0] = bin_octet + '0';
	}
}

void convert_ip_to_int(unsigned char *ip, unsigned int *int_ip)
{
	// octet will store in the format __0, __2, __3...._10, _11, _12.....101, 102...255
	unsigned char octet[3] = {STR_NULL, STR_NULL, STR_NULL};
	unsigned int octet_index = 2, octet_count = 0;
	unsigned int ip_len = mystrlen(ip);

	for (int i = 0; i <= ip_len; i++) {
		if (ip[i] == '.' || i == ip_len) {
			octet_count += 1;

			unsigned int bin_octet = 0;
			octet_to_int(octet, &bin_octet);

			// copy nth byte to the respective position (counting from left)
			if (octet_count == 1) {
				bin_octet = (bin_octet << 24);
				*int_ip = (*int_ip | bin_octet);
			}
			if (octet_count == 2) {
				bin_octet = (bin_octet << 16);
				*int_ip = (*int_ip | bin_octet);
			}
			if (octet_count == 3) {
				bin_octet = (bin_octet << 8);
				*int_ip = (*int_ip | bin_octet);
			}
			if (octet_count == 4) {
				*int_ip = (*int_ip | bin_octet);
			}

			octet[0] = STR_NULL, octet[1] = STR_NULL, octet[2] = STR_NULL;
			octet_index = 2;
		}
		else {
			octet[octet_index] = ip[i];
			octet_index -= 1;
		}
	}
}

void convert_ip_to_str(unsigned int *ip, unsigned char *str_ip)
{
	unsigned char bin_octet[4] = {
		*ip >> 24, (unsigned char)(*ip >> 16), (unsigned char)(*ip >> 8), (unsigned char)(*ip)
	};

	unsigned char str_octet[3] = {STR_NULL, STR_NULL, STR_NULL};

	// appending the octets into string IP one by one	
	my_itoa(bin_octet[0], str_octet);
	append_octet(str_ip, str_octet, '.');
	str_octet[0] = STR_NULL, str_octet[1] = STR_NULL, str_octet[2] = STR_NULL;

	my_itoa(bin_octet[1], str_octet);
	append_octet(str_ip, str_octet, '.');
	str_octet[0] = STR_NULL, str_octet[1] = STR_NULL, str_octet[2] = STR_NULL;

	my_itoa(bin_octet[2], str_octet);
	append_octet(str_ip, str_octet, '.');
	str_octet[0] = STR_NULL, str_octet[1] = STR_NULL, str_octet[2] = STR_NULL;

	my_itoa(bin_octet[3], str_octet);
	append_octet(str_ip, str_octet, STR_NULL);
}

void apply_mask(unsigned char *src_ip, char mask, unsigned char *dest_ip)
{
	unsigned int ip_addr = 0;
	convert_ip_to_int(src_ip, &ip_addr);

	unsigned int bin_mask = 0xffffffff << (32 - bin_mask);

	ip_addr = ip_addr & bin_mask;
	
	convert_ip_to_str(&ip_addr, dest_ip);
}

void fill_mac_with_broadcast(unsigned char *mac)
{
	mac[0] = 0xff;
	mac[1] = 0xff;
	mac[2] = 0xff;
	mac[3] = 0xff;
	mac[4] = 0xff;
	mac[5] = 0xff;
}
