CC=gcc
CFLAGS=-g
LIBS=-lpthread -lcli -L ./CommandParser
OBJS=libglue/glnode.o graph.o topologies.o network.o utils.o netcli.o
TARG=final.exe

final.exe: test.o ${OBJS} CommandParser/libcli.a
	${CC} ${CFLAGS} test.o ${OBJS} -o final.exe ${LIBS} 

test.o: test.c
	${CC} ${CFLAGS} -c test.c

glnode.o: libglue/glnode.c
	${CC} ${CFLAGS} -c libglue/glnode.c -o libglue/glnode.o

graph.o: graph.c
	${CC} ${CFLAGS} -c graph.c

topologies.o: topologies.c
	${CC} ${CFLAGS} -c topologies.c

network.o: network.c
	${CC} ${CFLAGS} -c network.c

netcli.o: netcli.c
	${CC} ${CFLAGS} -c netcli.c

CommandParser/libcli.a:
	(cd CommandParser; make)

all:
	make
	(cd CommandParser; make)

clean:
	rm *.o
	rm libglue/*.o
	rm final.exe
	(cd CommandParser; make clean)
	rm CMD_HIST_RECORD_FILE.txt
