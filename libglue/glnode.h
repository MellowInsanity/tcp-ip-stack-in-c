#ifndef GLNODE_H
#define GLNODE_H

#define OFFSET_OF(STRUCT, GLUE) (unsigned long long)(&((STRUCT *)0)->GLUE)

typedef struct glnode_ {
	struct glnode_ *left;
	struct glnode_ *right;
} glnode_t;

void init_glnode(glnode_t *);
void mystrcpy(unsigned char *, unsigned char *, unsigned int);
int mystrcmp(unsigned char *, unsigned char *, unsigned int);
unsigned int mystrlen(unsigned char *);

#endif
