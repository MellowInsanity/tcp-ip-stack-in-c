#include "glnode.h"
#include <stddef.h>

void init_glnode(glnode_t *glue)
{
	if (glue) {
		glue->left = NULL;
		glue->right = NULL;
	}
}

// size = sizeof(dest)-1, so no overflow possible
void mystrcpy(unsigned char *src, unsigned char *dest, unsigned int size)
{
	int si = 0, di = 0;
	while (si < size) {
		dest[di] = src[si];

		si += 1;
		di += 1;
	}
	dest[size] = '\0';
}

// size = sizeof(compare_to)-1, so no overflow possible
int mystrcmp(unsigned char *compare_from, unsigned char *compare_to, unsigned int size)
{
	for (int i = 0; i < size; i++) {
		if (compare_from[i] != compare_to[i])
			return 0;
	}
	return 1;
}

// assuming str is null terminated
unsigned int mystrlen(unsigned char *str)
{
	unsigned int len = 0;
	
	for (; str[len]; len++);

	return len;
}
