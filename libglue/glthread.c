#include <stdio.h>
#include <stdlib.h>
#include "glthread.h"

void print_db(glthread_t *list)
{
	if (!list->head) printf("list empty\n");

	glnode_t *iter = list->head;
	while (iter) {
		emp_t *e = (emp_t *)((unsigned long long int)(&(*iter)) - (unsigned long long int)list->offset);
		printf("name: %s, salary: %d, designation: %s, ID: %d\n", 
				e->name, e->salary, e->designation, e->empid);

		iter = iter->right;
	}
}

void init_list(glthread_t *list)
{
	list->head = NULL;
	list->tail = NULL;
	list->offset = OFFSET_OF(emp_t, glue);
}

glthread_t *create_list()
{
	glthread_t *list = calloc(1, sizeof(glthread_t));
	init_list(list);

	return list;
}

emp_t *init_emp(char n[32], unsigned int sal, char d[32], unsigned int id)
{
	emp_t *e = calloc(1, sizeof(emp_t));

	mystrcpy(n, e->name, MAX_NAME_SIZE-1);
	mystrcpy(d, e->designation, MAX_DESIG_SIZE-1);
	e->salary = sal;
	e->empid = id;

	return e;
}

void glthread_add(glthread_t *list, glnode_t *glue)
{
	if (!list->head || !list->tail) {
		list->head = glue;
		list->tail = glue;
	}
	else {
		list->tail->right = glue;
		glue->left = list->tail;
		list->tail = glue;
	}
}

void glthread_remove(glthread_t *list, glnode_t *glue)
{
	if (!list->head || !list->tail) {
		printf("can't remove from empty list!");
		return;
	}
	else {
		glnode_t *iter = list->head;
		while (iter) {
			if (glue == list->head) {
				list->head = list->head->right;
				printf("removed node\n");
				return;
			}
			if (glue == list->tail) {
				list->tail = list->tail->left;
				printf("removed node\n");
				return;
			}
			if (iter == glue) {
				glnode_t *prev = iter->left;
				glnode_t *next = iter->right;

				if (prev) prev->right = next;
				if (next) next->left = prev;

				printf("removed node\n");
				
				iter = NULL;

				free(iter);
				break;
			}
			iter = iter->right;
		}
	}
}
