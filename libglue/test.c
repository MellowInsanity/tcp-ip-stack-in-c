#include "glthread.h"
#include <stdlib.h>

int main()
{
	glthread_t *list = create_list();

	emp_t *e1 = init_emp("A", 1000, "aaa", 0);
	emp_t *e2 = init_emp("B", 2000, "bbb", 1);
	emp_t *e3 = init_emp("C", 3000, "ccc", 2);
	emp_t *e4 = init_emp("D", 4000, "ddd", 3);

	glthread_add(list, &e1->glue);
	glthread_add(list, &e2->glue);
	glthread_add(list, &e3->glue);
	glthread_add(list, &e4->glue);

	print_db(list);

	glthread_remove(list, &e1->glue);
	glthread_remove(list, &e2->glue);

	print_db(list);
	
	free(e1);
	free(e2);
	free(e3);
	free(e4);

	return 0;
}
