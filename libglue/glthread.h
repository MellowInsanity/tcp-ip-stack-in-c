#pragma once
#include "glnode.h"

#define MAX_NAME_SIZE 31
#define MAX_DESIG_SIZE 32

typedef struct glthread_ {
	struct glnode_ *head;
	struct glnode_ *tail;
	unsigned int offset;
} glthread_t;

typedef struct emp_ {
	char name[MAX_NAME_SIZE];
	unsigned int salary;
	char designation[MAX_DESIG_SIZE];
	unsigned int empid;
	glnode_t glue;
} emp_t;

void print_db(glthread_t *);
void init_list(glthread_t *);
glthread_t *create_list();
emp_t *init_emp(char[MAX_NAME_SIZE], unsigned int, char[MAX_DESIG_SIZE], unsigned int);
void glthread_add(glthread_t *, glnode_t *);
void glthread_remove(glthread_t *, glnode_t *);

