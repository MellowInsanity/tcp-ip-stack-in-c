#include "CommandParser/libcli.h"
#include "CommandParser/cmdtlv.h"
#include "cmdcodes.h"
#include "network.h"
#include "libglue/glnode.h"

#include <stdio.h>

extern glgraph_t *topo;

static int show_net_topology_handler(param_t *param, ser_buff_t *tlv_buffer, op_mode on_or_off)
{
	int CMDCODE = -1;
	CMDCODE = EXTRACT_CMD_CODE(tlv_buffer);

	switch(CMDCODE) {
		case CMD_CODE_SHOW_NET_TOPOLOGY:
			dump_net_graph(topo);
			break;
		default:
			;
	}
}

static int node_name_handler(param_t *param, ser_buff_t *tlv_buffer, op_mode on_of_off)
{
	/* not doing all the extra work to figure out node name because 'me lazy'... */
	printf("%s() is called...\n", __FUNCTION__);
	return 0;
}

static int arp_handler(param_t *param, ser_buff_t *tlv_buffer, op_mode on_of_off)
{
	printf("%s() is called...\n", __FUNCTION__);

	int cmd_code= EXTRACT_CMD_CODE(tlv_buffer);
	printf("cmd code: %d\n", cmd_code);

	/* we're doing all this extra work so as to figure out the actual params supplied by user */
	tlv_struct_t *tlv = NULL;
	char *node_name = NULL;
	char *ip_address = NULL;

	TLV_LOOP_BEGIN(tlv_buffer, tlv) {
		if (mystrcmp(tlv->leaf_id, "node_name", strlen("node_name"))) {
			node_name = tlv->value;
		}
		else if (mystrcmp(tlv->leaf_id, "ip_address", strlen("ip-address"))) {
			ip_address = tlv->value;
		}
	} TLV_LOOP_END;

	switch(cmd_code) {
		case CMD_CODE_RESOLVE_IP_ADDR:
			printf("Node Name: %s, IP Addresss: %s\n", node_name, ip_address);
			break;
		default:
			;
	}

	return 0;
}

int validate_node_name(char *value)
{
	if (mystrlen(value) >= NODE_NAME_SIZE || mystrlen(value) == 0) {
		printf("node name size too big!\n");
		return VALIDATION_FAILED;
	}
	
	printf("%s() called with value: %s\n", __FUNCTION__, value);

	return VALIDATION_SUCCESS;
}

int validate_ip_addr(char *ip)
{
	int a, b, c, d;
	int n = sscanf(ip, "%u.%u.%u.%u", &a, &b, &c, &d);

	if (n != 4 || a > 255 || b > 255 || c > 255 || d > 255) {
		printf("invalid IP entered!\n");
		return VALIDATION_FAILED;
	}

	printf("%s() called with value: %s\n", __FUNCTION__, ip);

	return VALIDATION_SUCCESS;
}

void init_net_cli()
{
	init_libcli();

	param_t *show 		= libcli_get_show_hook();
	param_t *debug 		= libcli_get_debug_hook();
	param_t *config 	= libcli_get_config_hook();
	param_t *run		= libcli_get_run_hook();
	param_t *debug_show = libcli_get_debug_show_hook();
	param_t *root 		= libcli_get_root();

	/* Command: show topology */
	{
		static param_t topology;
		init_param(&topology, CMD, "topology", show_net_topology_handler, 0, 
				INVALID, 0, "Usage: show topology");
		libcli_register_param(show, &topology);
		set_param_cmd_code(&topology, CMD_CODE_SHOW_NET_TOPOLOGY);
	}

	/* Full Command: run node <node name> arp-resolver <ip address> */
	{
		/* Command: run node */
		static param_t node;
		init_param(&node, CMD, "node", 0, 0, INVALID, NULL, "Usage: run node <node name>");
		libcli_register_param(run, &node);

		/* Command: run node <node name> */
		static param_t node_name;
		init_param(&node_name, LEAF, 0, node_name_handler, 
				validate_node_name, STRING, "node_name", "Usage: run node <node name>");
		libcli_register_param(&node, &node_name);

		/* Command: run node <node name> resolve-arp */
		static param_t resolve_arp;
		init_param(&resolve_arp, CMD, "resolve-arp", 0, 0, INVALID, 0, 
				"Usage: show node <node name> resolve-arp <ip address>");
		libcli_register_param(&node_name, &resolve_arp);

		/* Command: run node <node name> resolve-arp <ip address> */
		static param_t resolve_arp_ip_addr;
		init_param(&resolve_arp_ip_addr, LEAF, 0, arp_handler, 
				validate_ip_addr, IPV4, "ip_address", 
				"Usage: show node <node name> arp-resolver <ip address>");
		libcli_register_param(&resolve_arp, &resolve_arp_ip_addr);
		set_param_cmd_code(&resolve_arp_ip_addr, CMD_CODE_RESOLVE_IP_ADDR);
	}
	support_cmd_negation(config);

}

