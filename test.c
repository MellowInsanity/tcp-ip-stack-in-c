#include <stdio.h>
#include "network.h"
#include "CommandParser/libcli.h"

#define CLRSCR printf("\e[1;1H\e[2J");

// coming from topologies.c
extern glgraph_t *build_generic_graph();
// coming from netcli.c
extern void init_net_cli();

glgraph_t *topo = NULL;

int main()
{
	topo = build_generic_graph();
	init_net_cli();
	// CLRSCR;
	// dump_net_graph(topo);

	start_shell();

	return 0;
}
