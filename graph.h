#ifndef GRAPH_H
#define GRAPH_H

#include "libglue/glnode.h"

#define NODE_NAME_SIZE 16
#define TOPO_NAME_SIZE 32
#define INTF_NAME_SIZE 16
#define MAX_INTF_PER_NODE 10
#define GET_OTHER_INTF_PTR(intf) ((intf)->link->intf1 == (intf) ? \
									(intf)->link->intf2 : (intf)->link->intf1)

typedef struct node_ node_t;
typedef struct link_ link_t;

typedef struct node_net_prop_ node_net_prop_t;
typedef struct intf_net_prop_ intf_net_prop_t;

typedef struct interface_ {
	char intf_name[INTF_NAME_SIZE];
	node_t *src_node;
	link_t *link;
	intf_net_prop_t *net_prop;
} interface_t;

struct link_ {
	interface_t *intf1;
	interface_t *intf2;
	unsigned int cost;
};

struct node_ {
	char node_name[NODE_NAME_SIZE];
	interface_t *intf[MAX_INTF_PER_NODE];
	node_net_prop_t *net_prop;
	glnode_t glue;
};

typedef struct glgraph_ {
	char topology_name[TOPO_NAME_SIZE];
	glnode_t *head;
} glgraph_t;

#define NODE_GLUE_OFFSET OFFSET_OF(node_t, glue)

// initializers
node_t *create_node(char *);
interface_t *create_intf(char *, node_t *, link_t *);
link_t *create_link(char *, char *, node_t *, node_t *, unsigned int);
glgraph_t *create_glgraph(char *);
node_t *create_glgraph_node(glgraph_t *, char *);

// utility functions
void add_node_to_graph(glgraph_t *, glnode_t *);
node_t *get_other_node(interface_t *);
int get_available_intf_slot(node_t *);
interface_t *get_intf_by_name(node_t *, char *);
node_t *get_node_by_node_name(glgraph_t *, char *node_name);
void insert_link(node_t *, node_t *, char *, char *, unsigned int);
node_t *glue_to_node(glnode_t *);

// print routines
void dump_intf(interface_t *);
void dump_node(node_t *);
void dump_graph(glgraph_t *);

#endif
