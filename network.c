#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "utils.h"
#include "network.h"

#define PRINT_MAC(mac) printf("%x:%x:%x:%x:%x:%x\n", \
		(mac)[0], (mac)[1], (mac)[2], (mac)[3], (mac)[4], (mac)[5]);

void init_node_net_prop(node_net_prop_t **nnp)
{
	(*nnp) = calloc(1 ,sizeof(node_net_prop_t));

	(*nnp)->is_loopback_configured = false;
	// avoiding memset() as it requires a fucking header file of its own 
	// (this is not critical environment, I know it's a little unsafe)
	for (int i = 0; i < IPV4_ADDR_SIZE; i++)
		(*nnp)->loopback_addr.ip[i] = STR_NULL;	
}

void init_intf_net_prop(intf_net_prop_t **inp)
{
	(*inp) = calloc(1, sizeof(intf_net_prop_t));

	(*inp)->is_ip_configured = false;
	(*inp)->mask = 0;

	for (int i = 0; i < IPV4_ADDR_SIZE; i++)
		(*inp)->ip_addr.ip[i] = 0;

	for (int i = 0; i < MAC_ADDR_SIZE; i++)
		(*inp)->mac_addr.mac[i] = 0;
}

bool set_loopback(node_t *node, char *ip)
{
	assert(node);

	if (node->net_prop->is_loopback_configured) {
		printf("loopback for node %s is already configured\n", node->node_name);
	}

	node->net_prop->is_loopback_configured = 1;
	mystrcpy(ip, NODE_LOOPBACK(node), IPV4_ADDR_SIZE-1);

	return true;
}

bool set_intf_ip(node_t *node, char *target_intf_name, unsigned char *ip, char mask)
{
	assert(node);

	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (node->intf[i]) {
			printf("checking intf: %s\n", node->intf[i]->intf_name);
			if (mystrcmp(target_intf_name, node->intf[i]->intf_name, INTF_NAME_SIZE-1)) {
				interface_t *targ_intf = node->intf[i];	
				targ_intf->net_prop->mask = mask;

				mystrcpy(ip, INTF_IP(targ_intf), IPV4_ADDR_SIZE-1);
				printf("successfully set %s's IP\n", target_intf_name);

				return true;
			}
		}
	}

	printf("IP not set: %s interface not found!\n", target_intf_name);

	return false;
}

bool unset_intf_ip(node_t *node, char *targ_intf_name)
{
	assert(node);

	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (mystrcmp(targ_intf_name, node->intf[i]->intf_name, INTF_NAME_SIZE)) {
			interface_t *targ_intf = node->intf[i];	

			for (int i = 0; i < IPV4_ADDR_SIZE; i++)
				INTF_IP(targ_intf)[i] = 0;

			targ_intf->net_prop->is_ip_configured = 0;

			return true;
		}
	}

	return false;

}

void assign_mac_to_intf(interface_t *intf)
{
	assert(intf);
	assert(intf->net_prop);

	srand(time(0));
	for (int i = 0; i <= MAC_ADDR_SIZE; i++) {
		INTF_MAC(intf)[i] = rand() % 255;
	}

	printf("MAC assigned to %s\n", intf->intf_name);
}

interface_t *get_intf_with_matching_subnet(node_t *node, unsigned char *ip)
{
	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (node->intf[i]) {
			interface_t *other_intf = GET_OTHER_INTF_PTR(node->intf[i]);

			unsigned char other_masked_ip[IPV4_ADDR_SIZE];
			apply_mask(INTF_IP(other_intf), other_intf->net_prop->mask, other_masked_ip);

			unsigned char curr_masked_ip[IPV4_ADDR_SIZE];
			apply_mask(ip, node->intf[i]->net_prop->mask, curr_masked_ip);

			if (mystrcmp(other_masked_ip, curr_masked_ip, IPV4_ADDR_SIZE-1))
				return other_intf;
		}
	}

	return NULL;
}

void dump_net_intf(interface_t *intf)
{
	interface_t *other_intf = GET_OTHER_INTF_PTR(intf);
	printf("Intf Name: %s, ", intf->intf_name);
	printf("Intf MAC: ");
	PRINT_MAC(INTF_MAC(intf));

	if (intf->net_prop->is_ip_configured)
		printf("Intf IP: %s/%u\n", INTF_IP(intf), intf->net_prop->mask);
	else
		printf("IP not configured\n");

	printf("Connected Node Name: %s, Connected Intf Name: %s\n",
			other_intf->src_node->node_name, other_intf->intf_name);
	
	printf("Link Cost: %d\n", intf->link->cost);
}

void dump_net_node(node_t *node)
{
	printf("Node Name: %s, ", node->node_name);

	if (node->net_prop->is_loopback_configured)
		printf("Loopback: %s\n", (NODE_LOOPBACK(node)));
	else
		printf("loopback not configured\n");
	
	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (node->intf[i] != NULL) {
			dump_net_intf(node->intf[i]);
		}
		else {
			break;
		}
	}
}

void dump_net_graph(glgraph_t *gr)
{
	printf("Topology Name: %s\n\n", gr->topology_name);
	
	for (glnode_t *iter = gr->head; iter; iter = iter->right) {
		node_t *curr_node = glue_to_node(iter);
		dump_net_node(curr_node);
		printf("\n");
	}
}
