#ifndef UTILS_H
#define UTILS_H

#define IS_MAC_BROADCAST(mac) \
	((mac)[0] == 0xFF && (mac)[1] == 0xFF && (mac)[2] == 0xFF && (mac)[3] == 0xFF && \
	(mac)[4] == 0xFF && (mac)[5] == 0xFF)
#define MAC_ADDR_SIZE 6
#define IPV4_ADDR_SIZE 16
#define STR_NULL '\0'


// helper functions (basically alternatives to standard library functions)
void append_octet(unsigned char *, unsigned char *, unsigned char); 
void my_itoa(unsigned char, unsigned char *); // itoa()
void octet_to_int(unsigned char *, unsigned int *); // atoi()
void convert_ip_to_int(unsigned char *, unsigned int *);  // inet_pton()
void convert_ip_to_str(unsigned int *, unsigned char *); // inet_ntop()

// utility functions
void apply_mask(unsigned char *, char, unsigned char*);
void fill_mac_with_broadcast(unsigned char *);

#endif
