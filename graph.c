#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "graph.h"
#include "network.h"

node_t *create_node(char *node_name)
{
	node_t *n = calloc(1, sizeof(node_t));
	mystrcpy(node_name, n->node_name, NODE_NAME_SIZE-1);

	for (int i = 0; i < MAX_INTF_PER_NODE; i++)
		n->intf[i] = NULL;

	init_node_net_prop(&n->net_prop);
	init_glnode(&n->glue);

	printf("Node created with following details:\n");
	printf("Node name: %s\n", n->node_name);

	return n;
}

interface_t *create_intf(char *intf_name, node_t *src, link_t *link)
{
	interface_t *intf = calloc(1, sizeof(interface_t));

	intf->src_node = src;
	intf->link = link;
	mystrcpy(intf_name, intf->intf_name, INTF_NAME_SIZE-1);
	init_intf_net_prop(&intf->net_prop);

	printf("interface created with following details:\n");
	printf("intf name: %s, source node: %s\n", intf->intf_name, intf->src_node->node_name);
	return intf;
}

link_t *create_link(char *intf1, char *intf2, node_t *node1, node_t *node2, unsigned int cost)
{
	link_t *new_link = calloc(1, sizeof(link_t));

	new_link->intf1 = create_intf(intf1, node1, new_link);
	new_link->intf2 = create_intf(intf2, node2, new_link);

	assign_mac_to_intf(new_link->intf1);
	assign_mac_to_intf(new_link->intf2);

	new_link->cost = cost;

	return new_link;
}

glgraph_t *create_glgraph(char *topo_name)
{
	glgraph_t *gr = calloc(1, sizeof(glgraph_t));
	mystrcpy(topo_name, gr->topology_name, TOPO_NAME_SIZE-1);

	init_glnode(gr->head);
	gr->head = NULL;

	printf("Topology \"%s\" created.\n", topo_name);

	return gr;
}

node_t *create_glgraph_node(glgraph_t *gr, char *node_name)
{
	node_t *node = create_node(node_name);

	add_node_to_graph(gr, &node->glue);
	printf("Node \"%s\" created.\n", node_name);

	return node;
}

void add_node_to_graph(glgraph_t *gr, glnode_t *glue)
{
	if (!gr->head) {
		gr->head = glue;

		printf("\"%s\" appended to Topology \"%s\" as head\n", 
		    glue_to_node(glue)->node_name, gr->topology_name);
		return;
	}
	
	glnode_t *iter = gr->head;

	while (iter)
	{
		if (iter->right) {
			iter = iter->right;
		}
		else {
			iter->right = glue;
			glue->left = iter;
			
			break;
		}
	}
	printf("\"%s\" appended to Topology \"%s\"\n", 
		    glue_to_node(glue)->node_name, gr->topology_name);
}

interface_t *get_intf_by_name(node_t *node, char *intf_name)
{
	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (mystrcmp(node->intf[i]->intf_name, intf_name, INTF_NAME_SIZE)) {
			return node->intf[i];
		}
	}
	return NULL;
}

node_t *get_node_by_node_name(glgraph_t *gr, char *node_name)
{
	if (!gr->head)
		return NULL;
	
	for (glnode_t *iter = gr->head; iter; iter = iter->right)
	{
		node_t *curr_node = glue_to_node(iter);

		if (mystrcmp(curr_node->node_name, node_name, NODE_NAME_SIZE))
			return curr_node;
	}

	return NULL;
}

node_t *get_other_node(interface_t *intf)
{
	if (intf->link->intf1 == intf) 
		return intf->link->intf2->src_node;
	else 
		return intf->link->intf1->src_node;
}

int get_available_intf_slot(node_t *node)
{
	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (node->intf[i] == 0) 
			return i;
	}
}

void insert_link(node_t *node1, node_t *node2, char *intf_from, char *intf_to, unsigned int cost)
{
	link_t *link = create_link(intf_from, intf_to, node1, node2, cost);

	// reference node interface pointer with link interface pointer
	int intf_empty_slot = get_available_intf_slot(node1);
	node1->intf[intf_empty_slot] = link->intf1;

	intf_empty_slot = get_available_intf_slot(node2);
	node2->intf[intf_empty_slot] = link->intf2;
}

node_t *glue_to_node(glnode_t *glue)
{
	// welcome to C typecasting 101
	return ((node_t *)((unsigned long long)glue - NODE_GLUE_OFFSET));
}

void dump_intf(interface_t *intf)
{
	interface_t *other_intf = GET_OTHER_INTF_PTR(intf);
	printf("Source Node: %s, Source Intf Name: %s, Connected Node Name: %s, Connected Intf Name: %s, "
			"Link Cost: %d\n", intf->src_node->node_name, intf->intf_name, other_intf->src_node->node_name, 
			other_intf->intf_name, intf->link->cost);
}

void dump_node(node_t *node)
{
	printf("Node Name: %s\n", node->node_name);

	for (int i = 0; i < MAX_INTF_PER_NODE; i++) {
		if (node->intf[i] != NULL)
			dump_intf(node->intf[i]);
		else 
			break;
	}
}

void dump_graph(glgraph_t *gr)
{
	printf("Topology Name: %s\n\n", gr->topology_name);

	for (glnode_t *iter = gr->head; iter; iter = iter->right) {
		node_t *node = glue_to_node(iter);
		dump_node(node);
	}
}
