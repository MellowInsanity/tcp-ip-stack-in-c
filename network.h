#ifndef NETWORK_H
#define NETWORK_H

#include <stdbool.h>
#include "graph.h"

#define IPV4_ADDR_SIZE 16
#define MAC_ADDR_SIZE 6

//  typedef struct node_ node_t;
//  typedef struct interface_ interface_t;
//  typedef struct glgraph_ glgraph_t;

typedef struct ip_addr_ {
	unsigned char ip[IPV4_ADDR_SIZE];
} ip_addr_t;

typedef struct mac_addr_ {
	unsigned char mac[MAC_ADDR_SIZE];
} mac_addr_t;

typedef struct node_net_prop_ {
	ip_addr_t loopback_addr;
	bool is_loopback_configured;
} node_net_prop_t;

typedef struct intf_net_prop_ {
	ip_addr_t ip_addr;
	mac_addr_t mac_addr;
	bool is_ip_configured;
	unsigned char mask;
} intf_net_prop_t;

// macros to access common stuff, from "far away" (get it? reference to "So Far Away" song xD) 
#define INTF_MAC(intf) ((intf)->net_prop->mac_addr.mac)
#define INTF_IP(intf) ((intf)->net_prop->ip_addr.ip)
#define NODE_LOOPBACK(node) ((node)->net_prop->loopback_addr.ip)
#define IS_INTF_L3(intf) ((intf)->net_prop->is_ip_configured ? 1 : 0)

// initializers
void init_node_net_prop(node_net_prop_t **);
void init_intf_net_prop(intf_net_prop_t **);

// utility functions
bool set_loopback(node_t *, char *);
bool set_intf_ip(node_t *, char *, unsigned char *, char);
bool unset_intf_ip(node_t *, char *);
void assign_mac_to_intf(interface_t *);
interface_t *get_matching_subnet_intf(node_t *, unsigned char *);

// print routines which prints normal stuff + networking properties
void dump_net_intf(interface_t *);
void dump_net_node(node_t *);
void dump_net_graph(glgraph_t *);

#endif
